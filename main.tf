resource "azurerm_network_security_group" "myterraformnsg" {
  name = "myNetworkSecurityGroup"
  location = "eastus"
  resource_group_name = "${var.ressource_group_name}"

  security_rule {
    name = "SSH"
    priority = 1001
    direction = "Inbound"
    access = "Allow"
    protocol = "Tcp"
    source_port_range = "*"
    destination_port_range = "22"
    source_address_prefix = "*"
    destination_address_prefix = "*"
  }
}


data "azurerm_subnet" "Subnet" {
  name = "mySubnet"
  resource_group_name = "${var.ressource_group_name}"
  virtual_network_name = "myVnet"
}

   resource "azurerm_network_interface" "myterraformnic" {
     name = "myNIC${count.index}"
     count = "${length(var.ip_addresses)}"
     location = "${var.location}"
     resource_group_name = "${var.ressource_group_name}"
     ip_configuration {
       name = "myNicConfiguration"
       subnet_id = "${data.azurerm_subnet.Subnet.id}"
       private_ip_address_allocation = "static"
       private_ip_address = "${element(var.ip_addresses, count.index)}"
     }
   }


resource "azurerm_storage_account" "mystorageaccount" {
  name = "diag088c65a9be665fa8"
  resource_group_name = "${var.ressource_group_name}"
  location = "eastus"
  account_replication_type = "LRS"
  account_tier = "Standard"

  tags {
    environment = "Terraform Demo"
  }
}

  resource "azurerm_virtual_machine" "myterraformvm" {
  name = "devops-app${count.index}"
  location = "${var.location}"
  resource_group_name = "${var.ressource_group_name}"
  network_interface_ids = ["${element(azurerm_network_interface.myterraformnic.*.id, count.index)}"]
  vm_size = "Standard_DS1_v2"
  count = "${length(var.ip_addresses)}"
  storage_os_disk {
    name = "myOsDisk${count.index}"
    caching = "ReadWrite"
    create_option = "FromImage"
    managed_disk_type = "Premium_LRS"
  }
  storage_image_reference {
    publisher = "Canonical"
    offer = "UbuntuServer"
    sku = "16.04.0-LTS"
    version = "latest"
  }
  os_profile {
    computer_name = "devops-app${count.index}"
    admin_username = "stage"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path = "${var.ssh_path}"
      key_data = "${var.ssh_public_key}"
    }
  }
    boot_diagnostics {
      enabled = "true"
      storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
    }

    tags {
      environment = "Terraform Demo"
    }

}








  resource "azurerm_resource_group" "myterraformgroup5" {
    name = "myResourceGroup5"
    location = "northeurope"
    tags {
      environment = "Terraform Demo"
    }
  }
  resource "azurerm_virtual_network" "myterraformnetwork5" {
    name = "myVnet5"
    address_space = ["10.0.0.0/16"]
    location = "northeurope"
    resource_group_name = "${azurerm_resource_group.myterraformgroup5.name}"

    tags {
      environment = "Terraform Demo"
    }
  }
  resource "azurerm_public_ip" "myterraformpublicip5" {
    name = "myPublicIP5"
    location = "northeurope"
    resource_group_name = "${azurerm_resource_group.myterraformgroup5.name}"
    public_ip_address_allocation = "dynamic"
    domain_name_label = "team-mad5"

    tags {
      environment = "Terraform Demo"
    }
  }
  resource "azurerm_subnet" "myterraformsubnet5" {
    name = "mySubnet5"
    resource_group_name = "${azurerm_resource_group.myterraformgroup5.name}"
    virtual_network_name = "${azurerm_virtual_network.myterraformnetwork5.name}"
    address_prefix = "10.0.2.0/24"
  }
  resource "azurerm_network_security_group" "myterraformnsg5" {
    name = "myNetworkSecurityGroup5"
    location = "northeurope"
    resource_group_name = "${azurerm_resource_group.myterraformgroup5.name}"

    security_rule {
      name = "SSH"
      priority = 1005
      direction = "Inbound"
      access = "Allow"
      protocol = "Tcp"
      source_port_range = "*"
      destination_port_range = "22"
      source_address_prefix = "*"
      destination_address_prefix = "*"
    }
        
      security_rule {
      name = "HTTP"
      priority = 1006
      direction = "Inbound"
      access = "Allow"
      protocol = "Tcp"
      source_port_range = "*"
      destination_port_ranges = ["8080","80"]
      source_address_prefix = "*"
      destination_address_prefix = "*"
      }

    tags {
      environment = "Terraform Demo"
    }
  }
  resource "azurerm_network_interface" "myterraformnic5" {

    name = "myNIC5"
    location = "northeurope"
    resource_group_name = "${azurerm_resource_group.myterraformgroup5.name}"

    ip_configuration {
      name = "myNicConfiguration5"
      subnet_id = "${azurerm_subnet.myterraformsubnet5.id}"
      private_ip_address_allocation = "dynamic"
      public_ip_address_id = "${azurerm_public_ip.myterraformpublicip5.id}"
    }

    tags {
      environment = "Terraform Demo"
    }
  }
  resource "random_id" "randomId5" {
    keepers = {
      # Generate a new ID only when a new resource group is defined
      resource_group = "${azurerm_resource_group.myterraformgroup5.name}"
    }

    byte_length = 8
  }
  resource "azurerm_storage_account" "mystorageaccount5" {
    name = "diag${random_id.randomId5.hex}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup5.name}"
    location = "northeurope"
    account_replication_type = "LRS"
    account_tier = "Standard"

    tags {
      environment = "Terraform Demo"
    }
  }
  resource "azurerm_virtual_machine" "myterraformvm5" {
    name = "devops-app-05"
    location = "northeurope"
    resource_group_name = "${azurerm_resource_group.myterraformgroup5.name}"
    network_interface_ids = ["${azurerm_network_interface.myterraformnic5.id}"]
    vm_size = "Standard_DS1_v2"

    storage_os_disk {
      name = "myOsDisk5"
      caching = "ReadWrite"
      create_option = "FromImage"
      managed_disk_type = "Premium_LRS"
    }

    storage_image_reference {
      publisher = "Canonical"
      offer = "UbuntuServer"
      sku = "16.04.0-LTS"
      version ="latest"
    }

    os_profile {
      computer_name = "devops-app-05"
      admin_username = "stage"
    }




  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path = "${var.ssh_path}"
      key_data = "${var.ssh_public_key}"
    }
  }
    boot_diagnostics {
      enabled = "true"
      storage_uri = "${azurerm_storage_account.mystorageaccount5.primary_blob_endpoint}"
    }

    tags {
      environment = "Terraform Demo"
    }

}













